### Portfolio

My portfolio site. Built using React, Parcel, Bulma, and Sass. Continuous integration and hosting provided by GitLab;
HTTPS configured through LetsEncrypt and CloudFlare. [react-slick](https://github.com/akiran/react-slick) is used for the photo carousel component.
The Parcel configuration includes a basic service worker.

### Using this Project

1. Run `npm install`
2. `npm start` will run the project in development mode.
  * node-sass will compile the contents of `src/styles.scss` to `src/styles.css` and remain in watch mode
  * parcel will load `index.html` and the site will be available at `localhost:3000`
3. `npm run build` will build the project for deployment.
  * node-sass will compile the scss files.
  * parcel will output the build to `dist/`

GitLab will serve the contents of `dist/` and `public/` directories on deployment.