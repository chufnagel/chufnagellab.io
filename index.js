import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';

import './src/styles.scss';
import App from './src/app';

/* global window, document */

ReactGA.initialize('UA-124866161-1');

ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(<App />, document.getElementById('root'));


if (module.hot) {
  module.hot.accept();
}
