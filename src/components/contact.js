import React from 'react';

const Contact = () => (
  <section id="contact" className="section">
    <div id="column">
      <div className="tile is-vertical box">
        <h1 className="title">
          Contact
        </h1>
        <div className="columns is-centered">
          <div className="column">
            <a href="https://www.linkedin.com/in/charliehufnagel" rel="noopener noreferrer" target="_blank" className="button is-primary is-focused is-medium">
              <span className="icon">
                <i className="fab fa-linkedin" />
              </span>
              <span>LinkedIn</span>
            </a>
          </div>
          <div className="column">
            <a href="assets/Resume.pdf" className="button is-primary is-focused is-medium" download>
              <span className="icon">
                <i className="fas fa-file-download" />
              </span>
              <span>Resume</span>
            </a>
          </div>
          <div className="column">
            <a href="https://www.github.com/chufnagel" rel="noopener noreferrer" target="_blank" className="button is-primary is-focused is-medium">
              <span className="icon">
                <i className="fab fa-github" />
              </span>
              <span>GitHub</span>
            </a>
          </div>
          <div className="column">
            <a href="https://www.gitlab.com/chufnagel" rel="noopener noreferrer" target="_blank" className="button is-primary is-focused is-medium">
              <span className="icon">
                <i className="fab fa-gitlab" />
              </span>
              <span>GitLab</span>
            </a>
          </div>
          <div className="column">
            <a href="mailto:charles.hufnagel@pm.me" rel="noopener noreferrer" target="_blank" className="button is-primary is-focused is-medium">
              <span className="icon">
                <i className="fas fa-envelope-open" />
              </span>
              <span>Email</span>
            </a>
          </div>
          <div className="column">
            <a href="tel:434-987-0270" rel="noopener noreferrer" target="_blank" className="button is-primary is-focused is-medium">
              <span className="icon">
                <i className="fas fa-phone" />
              </span>
              <span>Phone</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Contact;
