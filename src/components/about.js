/* eslint-disable no-trailing-spaces */
/* eslint-disable max-len */
import React from 'react';
import portrait from '../assets/portrait.jpg';

const About = () => (
  <section id="about" className="section">
    <div className="column">
      <div className="columns is-centered">
        <div className="column is-two-thirds">
          <div className="tile is-vertical box">
            <figure className="image">
              <img src={portrait} alt="Charles Hufnagel" />
            </figure>
            <h1 className="title">
              About
            </h1>
            <br />
            <p className="subtitle">
              I am Charles Hufnagel, a Philadelphia-based full-stack software engineer.
              I am the Asia-Pacific team lead at STRATIS IoT.
            
              STRATIS makes shared, secure gateways for communicating with smart devices over Bluetooth, LoRa, Z-Wave, and Zigbee Networks.
            
              Rather than each individual occupant having to purchase an expensive gateway, a small set of gateways is spread throughout an apartment complex.
            
              My primary responsibilities involve server-side Python, Kafka and Kubernetes development/administration, and embedded devices.
              Areas of interest include continuous integration, container orchestration, stream-processing, and techno music.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default About;
