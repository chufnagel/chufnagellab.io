import React from 'react';

const Skills = () => (
  <section id="skills" className="section">
    <div className="column">
      <div className="columns is-centered is-multiline box is-one-quarter">
        <div className="column">
          <h1 className="title">
            What I Do
          </h1>
          <br />
          <div className="container box">
            <p className="subtitle is-5">
              Strong:
            </p>
            <ul>
              <li>Functional and Object-Oriented Programming</li>
              <li>JavaScript, Python, SQL</li>
              <li>Test-Driven Development</li>
              <li>API Design</li>
              <li>AWS and Kubernetes Cluster Administration</li>
              <li>Continuous Integration / Deployment</li>
              <li>Monitoring and Alerting Infrastructure</li>
              <li>React, Redux</li>
              <li>Kafka / ZooKeeper</li>
              <li>Django, Express, Nginx</li>
              <li>MySQL, MongoDB, Postgres</li>
              <li>Redis, Memcached</li>
              <li>Elastic Stack</li>
              <li>Web App Bundlers: Webpack, Parcel</li>
              <li>JavaScript Test Suites: Chai, Enzyme, Jest, Mocha</li>
            </ul>
          </div>
          <br />
          <div className="container box">
            <p className="subtitle is-5">
              Experienced:
            </p>
            <ul>
              <li>Golang, Java, Scala</li>
              <li>TypeScript</li>
              <li>Angular 2+</li>
              <li>Apache Spark</li>
              <li>Google App Engine</li>
              <li>Agile Methodology</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Skills;
