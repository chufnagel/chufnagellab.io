import React from 'react';
import Slider from 'react-slick';

import attractions from '../assets/wanderer_attractions.png';
import photos from '../assets/wanderer_photos.png';
import search from '../assets/wanderer_search_results.png';
import search2 from '../assets/wanderer_search_results_2.png';
import favorites from '../assets/yinder_favorites.png';
import landing from '../assets/yinder_landing.png';


const Projects = () => {
  const settings = {
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: 'linear',
    dots: true,
    infinite: true,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  const wandererImages = [attractions, photos, search, search2];
  const yinderImages = [favorites, landing];
  const wanderer = (
    <Slider {...settings}>
      {wandererImages.map(image => (
        <img key={image} src={image} alt="A view from the Wanderer App" />
      ))}
    </Slider>
  );
  const yinder = (
    <Slider {...settings}>
      {yinderImages.map(image => (
        <img key={image} src={image} alt="A view from the Yinder App" />
      ))}
    </Slider>
  );
  return (
    <section id="projects" className="section">
      <div className="column">
        <div className="columns">
          <div className="container">
            <div className="column">
              <div className="tile is-vertical box is-parent">
                <h1 className="title">
                  Projects
                </h1>
                <div className="tile is-child box">
                  {wanderer}
                  <br />
                  <p className="subtitle">
                    <a href="http://68.183.130.27:8080/" rel="noopener noreferrer" target="_blank">
                      Wanderer
                    </a>
                  </p>
                  <div>
                    <p>
                      A social travel diary/experience finder.
                      Created with React, Redux, Redux-Saga, Material-UI,
                      and MariaDB. Site runs on AWS Elastic Container Service
                      and Express with an Nginx reverse proxy and is secured
                      with LetsEncrypt. End-to-end and unit test suites built
                      with Enzyme, Jest, Mocha, and Selenium WebDriver.
                    </p>
                  </div>
                </div>
                <div className="column">
                  <div className="tile is-child box">
                    {yinder}
                    <br />
                    <p className="subtitle">
                      <a href="https://www.yelp-tinder.appspot.com" rel="noopener noreferrer" target="_blank">
                        Yinder
                      </a>
                    </p>
                    <div>
                      <p>
                        A Tinder-style restaurant finder.
                        Built using React, Materialize-CSS, MongoDB, and
                        Express. Containerized with Docker, tested with Ava,
                        hosted on Google Cloud Platform. Leverages the Yelp Fusion API.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Projects;
