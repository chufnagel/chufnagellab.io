import React from 'react';
import About from './components/about';
import Contact from './components/contact';
import Projects from './components/projects';
import Skills from './components/skills';

const App = () => (
  <div className="App columns is-multiline is-half is-centered">
    <About />
    <Projects />
    <Skills />
    <Contact />
    <div className="column is-full">
      <div className="columns is-centered">
        <div className="column">
          <footer className="footer">
            <a href="#">Back To Top</a>
          </footer>
        </div>
      </div>
    </div>
  </div>
);

export default App;
